'use strict';

/**
 * @ngdoc function
 * @name angularNewUi2App.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the angularNewUi2App
 */
angular.module('angularNewUi2App')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
