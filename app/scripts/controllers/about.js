'use strict';

/**
 * @ngdoc function
 * @name angularNewUi2App.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the angularNewUi2App
 */
angular.module('angularNewUi2App')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
