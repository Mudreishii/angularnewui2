angular.module('angularNewUi2App')
  .directive('mainMenu', function(){
    return {
      restrict: 'E',
      templateUrl: 'views/mainMenu/list.html'
    };
  });