'use strict';

/**
 * @ngdoc overview
 * @name angularNewUi2App
 * @description
 * # angularNewUi2App
 *
 * Main module of the application.
 */
angular
  .module('angularNewUi2App', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ui.router',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("main");
    $stateProvider
//      .when('/main', {
//        templateUrl: 'views/main.html',
//        controller: 'MainCtrl',
//        controllerAs: 'main'
//      })
//      .when('/about', {
//        templateUrl: 'views/about.html',
//        controller: 'AboutCtrl',
//        controllerAs: 'about'
//      })
      .state('main', {
        url: "/main",
        templateUrl: "views/main.html"

      })
        .state('main.list', {
          url: "/list",
          templateUrl: "views/main.list.html"
        })
      .state('about', {
        url: "/about",
        templateUrl: "views/about.html"
//        controller: 'about'
      });

  });
